package org.ludo;

import java.text.Normalizer;
import java.util.regex.Pattern;

public class SuppressionAccents {
    /*
        Fonction qui reçoit une chaine de caractère en paramètre et qui retourne cette chaine sans accents
     */
    public static String sansAccent(String phrase){
        String phrase_temporaire = Normalizer.normalize(phrase, Normalizer.Form.NFD);
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        phrase_temporaire = pattern.matcher(phrase_temporaire).replaceAll("");
        phrase_temporaire = phrase_temporaire.toLowerCase();  // Suppression majuscules
        return phrase_temporaire;
    }
    public static int compteur_voyelles(String phrase){
        /*
            Fonction qui reçoit la chaîne de caractère nettoyée (sans accents, sans majuscules)
            et qui renvoit le nombre de voyelles
         */
        int nombre_voyelle = 0;
        for(int i = 0; i < phrase.length(); i++) {
            char caractere = phrase.charAt(i);
            if (caractere == 'a' || caractere == 'e' || caractere == 'i' || caractere == 'o' || caractere == 'u') {
                nombre_voyelle++;
            }
        }
        return nombre_voyelle;
    }

    
}