package org.ludo;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        // Création du input
        Scanner myObj = new Scanner(System.in);
        System.out.println("Entrez votre phrase:");
        // Récupération du input
        String phrase = myObj.nextLine();
        // Appel de la classe SuppressionAccents
        SuppressionAccents myObj1 = new SuppressionAccents();
        // Suppression des accents et majuscules
        String sansAccents= myObj1.sansAccent(phrase);
        // Fonction qui compte les voyelles
        int nbr_voyelles = myObj1.compteur_voyelles(sansAccents);
        // Renvoie le nombre de voyelles
        System.out.printf("Il y a  %d voyelle(s) \n", nbr_voyelles);


    }


}
